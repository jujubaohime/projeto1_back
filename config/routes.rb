Rails.application.routes.draw do
  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', password: 'senha', confirmation: 'verificacao', unlock: 'desbloquear', registration: 'registro', sign_up: 'cadastro' }
  get 'pages/home'
  root to: "pages#home"
  resources :equivalences
  resources :course_subjects
  resources :presences
  resources :subscriptions
  resources :users
  resources :lessons
  resources :classrooms
  resources :subjects
  resources :periods
  resources :students
  resources :courses
  resources :professors
  resources :departments
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
