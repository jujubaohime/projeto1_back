json.extract! presence, :id, :lesson_id, :student_id, :created_at, :updated_at
json.url presence_url(presence, format: :json)
