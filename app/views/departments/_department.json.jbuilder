json.extract! department, :id, :name, :initials, :created_at, :updated_at
json.url department_url(department, format: :json)
