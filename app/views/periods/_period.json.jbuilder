json.extract! period, :id, :semester, :created_at, :updated_at
json.url period_url(period, format: :json)
