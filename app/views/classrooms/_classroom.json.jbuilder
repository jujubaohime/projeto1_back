json.extract! classroom, :id, :subject_id, :professor_id, :period_id, :code, :created_at, :updated_at
json.url classroom_url(classroom, format: :json)
