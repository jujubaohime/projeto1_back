json.extract! professor, :id, :department_id, :name, :age, :created_at, :updated_at
json.url professor_url(professor, format: :json)
