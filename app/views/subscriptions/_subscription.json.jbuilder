json.extract! subscription, :id, :classroom_id, :student_id, :created_at, :updated_at
json.url subscription_url(subscription, format: :json)
