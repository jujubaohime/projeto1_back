json.extract! lesson, :id, :classroom_id, :date, :created_at, :updated_at
json.url lesson_url(lesson, format: :json)
