json.extract! student, :id, :course_id, :name, :age, :created_at, :updated_at
json.url student_url(student, format: :json)
