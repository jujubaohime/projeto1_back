json.extract! subject, :id, :department_id, :name, :created_at, :updated_at
json.url subject_url(subject, format: :json)
