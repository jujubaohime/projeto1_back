class Lesson < ApplicationRecord
  belongs_to :classroom
  has_many :presences
  has_many :students, through: :presences
end
