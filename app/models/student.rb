class Student < ApplicationRecord
  belongs_to :course
  has_many :subscriptions
  has_many :students, through: :subscriptions
  has_many :presences
  has_many :lessons, through: :presences
end
