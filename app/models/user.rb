class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
    has_one :professor
    validates :name, presence: true
    mount_uploader :photo, PhotoUploader


    enum kind: {
      standard: 0,
      admin: 1,
      professor: 2,
      diretor: 3,
    }
    private
      def must_be_above_18
        if age < 18
          error.add('base', "Precisa ter 18 anos ou mais")
        end
      end
end
