class Course < ApplicationRecord
  belongs_to :department
  has_many :students
  has_many :course_subjects
  has_many :subjects, through: :course_subjects
end
