class CreateEquivalences < ActiveRecord::Migration[5.2]
  def change
    create_table :equivalences do |t|
      t.references :subject, foreign_key: true
      t.references :equivalent_subject, index: true

      t.timestamps
    end
  end
end
