class RemoveInitialFromDepartment < ActiveRecord::Migration[5.2]
  def change
    remove_column :departments, :initial, :string
  end
end
