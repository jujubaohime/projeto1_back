class RemoveInitialsFromDepartment < ActiveRecord::Migration[5.2]
  def change
    remove_column :departments, :initials, :string
  end
end
