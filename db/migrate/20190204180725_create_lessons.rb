class CreateLessons < ActiveRecord::Migration[5.2]
  def change
    create_table :lessons do |t|
      t.references :classroom, foreign_key: true
      t.datetime :date

      t.timestamps
    end
  end
end
