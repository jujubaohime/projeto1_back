# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

10.times do |i|
    User.create!(name: Faker::RuPaul.queen, email: Faker::Internet.email, password: '123456', kind: 0)
    Department.create!(name: Faker::GameOfThrones.house)
    Period.create!(semester: "#{i}")
end

20.times do |i|
    Professor.create!(name: Faker::RuPaul.queen, age: rand(20..70), department_id: Department.ids.sample)
    Course.create!(name: Faker::Educator.course, department_id: Department.ids.sample)
    Subject.create!(name: Faker::HarryPotter.spell, department_id: Department.ids.sample)
end

20.times do |i|
    Classroom.create!(subject_id: Subject.ids.sample, professor_id: Professor.ids.sample, period_id: Period.ids.sample, code: rand(100..999))
    Student.create!(name: Faker::RuPaul.queen, age: rand(18..70), course_id: Course.ids.sample )
end

30.times do |i|
    Lesson.create!(classroom_id: Classroom.ids.sample, date: DateTime.strptime("01/14/2019 9:01", "%m/%d/%Y %H:%M"))
end



User.create!(name: "admin", email: "admin@admin.com", password:"123456", kind: 1)
User.create!(name: "user", email: "user@user.com", password:"123456", kind: 0)

